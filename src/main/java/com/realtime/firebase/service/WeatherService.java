package com.realtime.firebase.service;

import com.realtime.firebase.model.WeatherMapResponse;
import org.springframework.http.ResponseEntity;

public interface WeatherService {

    ResponseEntity<WeatherMapResponse> getWeatherMapInfo();
    ResponseEntity<String> getCurrentWeatherDescription();
    ResponseEntity<Double> getCurrentTemperature();
    ResponseEntity<Double> getCurrentHumidity();

    void saveWeatherMapData( );
    void saveWeatherMapFirebaseData();
    void runRequestSchedule(int time);
    void runFirebaseSchedule(int time);

}
