package com.realtime.firebase.service.impl;

import com.google.api.core.ApiFuture;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.realtime.firebase.FirebaseApplication;
import com.realtime.firebase.controller.WeatherController;
import com.realtime.firebase.entity.WeatherData;
import com.realtime.firebase.model.MainInfo;
import com.realtime.firebase.model.Weather;
import com.realtime.firebase.model.WeatherMapResponse;
import com.realtime.firebase.repository.WeatherRepository;
import com.realtime.firebase.service.WeatherService;
import com.realtime.firebase.util.HttpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.sql.Time;
import java.util.*;

@Service
@EnableScheduling
public class WeatherServiceImpl implements WeatherService {

    private HttpService httpService;

    private WeatherRepository weatherRepository;

    @Value("${weatherMapUrl}")
    String weatherMapUrl;

    public WeatherServiceImpl() {
    }

    @Autowired
    public WeatherServiceImpl(HttpService httpService, WeatherRepository weatherRepository) {
        this.httpService = httpService;
        this.weatherRepository = weatherRepository;
    }

    @Override
    public ResponseEntity<WeatherMapResponse> getWeatherMapInfo() {
        ResponseEntity<WeatherMapResponse> responseEntity = this.httpService.sendHttpGetWeather(weatherMapUrl);
        return new ResponseEntity<>(responseEntity.getBody(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> getCurrentWeatherDescription() {
        ResponseEntity<WeatherMapResponse> responseEntity = this.httpService.sendHttpGetWeather(weatherMapUrl);
        return new ResponseEntity<>(responseEntity.getBody().getWeather().get(0).getDescription(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Double> getCurrentTemperature() {
        ResponseEntity<WeatherMapResponse> responseEntity = this.httpService.sendHttpGetWeather(weatherMapUrl);
        return new ResponseEntity<>(responseEntity.getBody().getMain().getTemp() - 271.15, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Double> getCurrentHumidity() {
        ResponseEntity<WeatherMapResponse> responseEntity = this.httpService.sendHttpGetWeather(weatherMapUrl);
        return new ResponseEntity<>(responseEntity.getBody().getMain().getHumidity(), HttpStatus.OK);
    }


    @Override
//    @Scheduled(fixedRate = 1000)
    public void saveWeatherMapData() {

        //save weather data into mysql db
        WeatherMapResponse mapResponse = getWeatherMapInfo().getBody();
        WeatherData weatherData = new WeatherData();
        weatherData.setWeatherDescription(mapResponse.getWeather().get(0).getDescription());
        weatherData.setTemperature(mapResponse.getMain().getTemp() - 273.15);
        weatherData.setHumidity(mapResponse.getMain().getHumidity());
        weatherData.setCreatedDate(new Date());
        Double averagesTemp = weatherRepository.getAveragesTemp();
        Double averagesHumidity = weatherRepository.getAveragesHumidity();
        if (averagesTemp != null || averagesHumidity != null) {
            weatherData.setAverageTemperature(averagesTemp);
            weatherData.setAverageHumidity(averagesHumidity);
        } else {
            weatherData.setAverageTemperature(mapResponse.getMain().getTemp() - 273.15);
            weatherData.setAverageHumidity(mapResponse.getMain().getHumidity());
        }

        weatherRepository.save(weatherData);
    }

    @Override
//    @Scheduled(fixedRate = 1000)
    public void saveWeatherMapFirebaseData() {
        //firebase databse updating
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("weatherInfo");
        Map<String, WeatherMapResponse> weatherMap = new HashMap<>();
//        List<WeatherData> weatherList = weatherRepository.getLastXHourData(1);
        List<WeatherData> weatherList = weatherRepository.getLastWeatherData();
        for (WeatherData wer : weatherList) {

            List<Weather> weatherL = new ArrayList<>();
            Weather weather = new Weather();
            weather.setDescription(wer.getWeatherDescription());
            weatherL.add(weather);

            MainInfo main = new MainInfo();
            main.setTemp(wer.getTemperature());
            main.setHumidity(wer.getHumidity());
            main.setAvgTemp(wer.getAverageTemperature());
            main.setAvgHumidity(wer.getAverageHumidity());
            WeatherMapResponse weatherMapResponse = new WeatherMapResponse();
            weatherMapResponse.setMain(main);
            weatherMapResponse.setWeather(weatherL);
            weatherMap.put(wer.getId() + "", weatherMapResponse);
        }
        ref.setValueAsync(weatherMap);
    }

    public void runRequestSchedule(int time) {

        while (true) {
            this.saveWeatherMapData();
            System.out.println(new Date());
            try {
                Thread.sleep(time*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void runFirebaseSchedule(int time) {

        while (true) {
            this.saveWeatherMapFirebaseData();
            System.out.println(new Date());
            try {
                Thread.sleep(time*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
