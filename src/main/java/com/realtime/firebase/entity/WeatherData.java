package com.realtime.firebase.entity;

import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
public class WeatherData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    String weatherDescription;
    double temperature;
    double humidity;
    double averageTemperature;
    double averageHumidity;
    Date  createdDate;

    @Override
    public String toString() {
        return "WeatherData{" +
                "id=" + id +
                ", weatherDescription='" + weatherDescription + '\'' +
                ", temperature=" + temperature +
                ", humidity=" + humidity +
                ", averageTemperature=" + averageTemperature +
                ", averageHumidity=" + averageHumidity +
                ", createdDate=" + createdDate +
                '}';
    }
}
