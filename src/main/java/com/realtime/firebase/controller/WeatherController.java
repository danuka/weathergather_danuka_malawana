package com.realtime.firebase.controller;

import com.realtime.firebase.FirebaseApplication;
import com.realtime.firebase.model.WeatherMapResponse;
import com.realtime.firebase.service.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/weather")
@CrossOrigin("http://localhost:3000")
public class WeatherController {

//    public static int requestFrequency;
//    public static int updateFrequency;

    private WeatherService weatherService;

    public WeatherController() {
    }

    @Autowired
    public WeatherController(WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    @GetMapping("/getWeatherMap")
    public ResponseEntity<WeatherMapResponse> getWeatherMapInfo() {
        return weatherService.getWeatherMapInfo();
    }

    @GetMapping("/getCurrentWeatherDescription")
    public ResponseEntity<String> getCurrentWeatherDescription() {
        return weatherService.getCurrentWeatherDescription();
    }

    @GetMapping("/getCurrentTemperature")
    public ResponseEntity<Double> getCurrentTemperature() {
        return weatherService.getCurrentTemperature();
    }

    @GetMapping("/getCurrentHumidity")
    public ResponseEntity<Double> getCurrentHumidity() {
        return weatherService.getCurrentHumidity();
    }

    @PostMapping("/mapFrequency")
    public void setOpenWeatherMapFrequency(@RequestBody String reqFrequency) {
        weatherService.runRequestSchedule(Integer.parseInt(reqFrequency));
    }

    @PostMapping("/dbUpdateFrequency")
    public void setDBUpdateFrequency(@RequestBody String updateFreq) {
        weatherService.runFirebaseSchedule(Integer.parseInt(updateFreq));
    }


}
