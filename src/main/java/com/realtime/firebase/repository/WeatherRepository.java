package com.realtime.firebase.repository;

import com.realtime.firebase.entity.WeatherData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WeatherRepository extends JpaRepository<WeatherData, Integer> {

    @Query(value = "SELECT * FROM weather_data WHERE  created_date >= NOW()- INTERVAL ?1 HOUR", nativeQuery = true)
    List<WeatherData> getLastXHourData(int hours);

    @Query(value = "SELECT * FROM weather_data ORDER BY id DESC limit 5", nativeQuery = true)
    List<WeatherData> getLastWeatherData();

    @Query(value = "SELECT avg(temperature) FROM weather_data WHERE  created_date >= NOW()- INTERVAL 1 HOUR", nativeQuery = true)
    Double getAveragesTemp();

    @Query(value = "SELECT  avg(humidity) FROM weather_data WHERE  created_date >= NOW()- INTERVAL 1 HOUR", nativeQuery = true)
    Double getAveragesHumidity();


}
