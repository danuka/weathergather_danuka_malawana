package com.realtime.firebase.model;

import com.realtime.firebase.entity.WeatherData;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ModelMapper {

    WeatherData convertToWeatherEntity(WeatherDataDto weatherDataDto) {
        WeatherData weatherData = new WeatherData();
        weatherData.setId(weatherDataDto.getId());
        weatherData.setWeatherDescription(weatherDataDto.getWeatherDescription());
        weatherData.setTemperature(weatherDataDto.getTemperature());
        weatherData.setHumidity(weatherDataDto.getHumidity());
        weatherData.setAverageTemperature(weatherDataDto.getAverageTemperature());
        weatherData.setAverageHumidity(weatherDataDto.getAverageHumidity());
        weatherData.setCreatedDate(weatherDataDto.getCreatedDate());
        return weatherData;
    }


    WeatherDataDto convertToWeatherDto(WeatherData weatherData) {
        WeatherDataDto weatherDataDto = new WeatherDataDto();
        weatherDataDto.setId(weatherData.getId());
        weatherDataDto.setWeatherDescription(weatherData.getWeatherDescription());
        weatherDataDto.setTemperature(weatherData.getTemperature());
        weatherDataDto.setHumidity(weatherData.getHumidity());
        weatherDataDto.setAverageTemperature(weatherData.getAverageTemperature());
        weatherDataDto.setAverageHumidity(weatherData.getAverageHumidity());
        weatherDataDto.setCreatedDate(weatherData.getCreatedDate());
        return weatherDataDto;
    }

    List<WeatherData> converttoWeatherEntityList(List<WeatherDataDto> weatherDataDtos) {
        List<WeatherData> weatherDataList = new ArrayList<>();
        weatherDataDtos.forEach(wDto -> {
            weatherDataList.add(convertToWeatherEntity(wDto));
        });
        return weatherDataList;
    }


    List<WeatherDataDto> converttoWeatherDtoList(List<WeatherData> weatherDataEntities) {
        List<WeatherDataDto> weatherDataList = new ArrayList<>();
        weatherDataEntities.forEach(wDto -> {
            weatherDataList.add(convertToWeatherDto(wDto));
        });
        return weatherDataList;
    }

}
