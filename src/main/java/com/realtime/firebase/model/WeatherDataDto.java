package com.realtime.firebase.model;

import lombok.Data;

import java.util.Date;

@Data
public class WeatherDataDto {
    int id;
    String weatherDescription;
    double temperature;
    double humidity;
    double averageTemperature;
    double averageHumidity;
    Date createdDate;

    @Override
    public String toString() {
        return "WeatherDataDto{" +
                "id=" + id +
                ", weatherDescription='" + weatherDescription + '\'' +
                ", temperature=" + temperature +
                ", humidity=" + humidity +
                ", averageTemperature=" + averageTemperature +
                ", averageHumidity=" + averageHumidity +
                ", createdDate=" + createdDate +
                '}';
    }
}
