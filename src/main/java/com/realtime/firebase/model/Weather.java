package com.realtime.firebase.model;

import lombok.Data;

@Data

public class Weather {
    private String description;

}
