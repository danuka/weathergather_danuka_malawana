package com.realtime.firebase.model;


import lombok.Data;

import java.util.List;

@Data
public class WeatherMapResponse {

    private List<Weather> weather;
    private MainInfo main;


}







