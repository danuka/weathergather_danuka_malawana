package com.realtime.firebase.model;

import lombok.Data;

@Data
public class MainInfo {
    private double temp;
    private double humidity;
    private double avgTemp;
    private double avgHumidity;

}
